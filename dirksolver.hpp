#pragma once
#include <Eigen/Core>
#include <vector>
#include "coronaoutbreak.hpp"

using VXd = Eigen::VectorXd;
using MXd = Eigen::MatrixXd;

static constexpr int dimens = 4;

class DIRKSolver {
public:
	DIRKSolver(const CoronaOutbreak& coronaOutbreak_)
		: coronaOutbreak(coronaOutbreak_) {

	}

	inline VXd
	F(double t, VXd& y) {
	  Eigen::VectorXd ret(dimens);
	  this->coronaOutbreak.computeF(ret, t, y);
	  return ret.eval();
	}

	///
	/// Evaluates function G1(y) from task b)
	/// @param[out] G evaluation of function
	/// @param[in] y input to G
	/// @param[in] tn current time
	/// @param[in] Un computed value of U = [S,E,I,R] at time tn
	/// @param[in] dt timestep
	///
	//----------------G1Start----------------
	void computeG1(Eigen::VectorXd& G, Eigen::VectorXd& y, double tn,
		Eigen::VectorXd& Un, double dt) {
	  G = -y + Un + dt * mu * F(tn + mu * dt, y);
	}
	//----------------G1End----------------

	///
	/// Evaluates function G2(y1, y) from task b)
	/// @param[out] G evaluation of function
	/// @param[in] y input to G
	/// @param[in] tn current time
	/// @param[in] Un computed value of U = [S,E,I,R] at time tn
	/// @param[in] dt timestep
	/// @param[in] y1 computed value for first intermediate RK value
	///
	void computeG2(Eigen::VectorXd& G, Eigen::VectorXd& y, double tn,
		Eigen::VectorXd Un, double dt, Eigen::VectorXd& y1) {
	  G = -y + Un + dt * (-nu * F(tn + mu*dt, y1)
	  					  +mu * F(tn + (mu - nu)*dt, y));
	}

	inline MXd 
	JG1(VXd& U, double tn, double dt) {
	  MXd ret(4,4);
	  coronaOutbreak.computeJF(ret, tn + mu*dt, U);
	  ret = -MXd::Identity(4, 4) + mu * dt * ret;
	  return ret.eval(); 
	}

	inline MXd 
	JG2(VXd& U, double tn, double dt) {
	  MXd ret(4,4);
	  coronaOutbreak.computeJF(ret, tn + (mu - nu) * dt, U);
	  ret = -MXd::Identity(4, 4) + mu * dt * ret;
	  return ret.eval(); 
	}

	///
	/// Find a solution to JG1*x = -G1 with Newton's method
	/// @param[out] x solution to the system
	/// @param[in] Un computed value of U = [S,E,I,R] at time tn
	/// @param[in] dt timestep
	/// @param[in] tn current time
	/// @param[in] tolerance if Newton increment smaller, successfully converged
	/// @param[in] maxIterations  max Newton iterations to try before failing
	///
	void newtonSolveY1(Eigen::VectorXd& u, Eigen::VectorXd& Un, double dt,
		double tn, double tolerance, int maxIterations) {

		VXd z(dimens);
		int i = 0;

	  	auto G1 = [&](VXd& y) -> VXd {
			  	VXd ret(dimens);
			  	this->computeG1(ret, y, tn, Un, dt);
			  	return ret.eval(); 
			  };


		u = Un.eval();
	
		while (i++ < maxIterations) {
		  z = JG1(u, tn, dt).lu().solve(-G1(u));
		  u = u + z;
		  if(z.norm() < tolerance)
			return;
	 	}

		throw std::runtime_error("Did not reach tolerance in Newton iteration in Y1");
	}

	///
	/// Find a solution to JG2*x = -G2 with Newton's method
	/// @param[out] x solution to the system
	/// @param[in] Un computed value of U = [S, E, I, R] at time tn
	/// @param[in] y1 previous intermediate value for RK method
	/// @param[in] dt timestep
	/// @param[in] tn current time
	/// @param[in] tolerance if Newton increment smaller, successfully converged
	/// @param[in] maxIterations  max Newton iterations to try before failing
	///
	//----------------NewtonG2Start----------------
	void newtonSolveY2(Eigen::VectorXd& v, Eigen::VectorXd& Un,
		Eigen::VectorXd& y1, double dt, double tn, double tolerance, int maxIterations) {

		VXd z(dimens);
		int i = 0;

	  	auto G2 = [&](VXd y) -> VXd {
			  	VXd ret(dimens);
			  	this->computeG2(ret, y, tn, Un, dt, y1);
			  	return ret.eval(); 
			  };

		v = Un.eval();

		while (i++ < maxIterations) {
		  z = JG1(v, tn, dt).lu().solve(-G2(v));
		  v = v + z;
		  if(z.norm() < tolerance)
			return;
	 	}

		throw std::runtime_error("Did not reach tolerance in Newton iteration in Y1");
	}
	//----------------NewtonG2End----------------

    inline VXd 
	dirk23_step(VXd& un, double t, double dt) {
	  VXd y1(dimens), y2(dimens);
	  //char buf[2*sizeof(VXd)]

	  auto G = [&](VXd& x1, VXd& x2) -> VXd {
			  VXd ret =  un + dt * (mu - 0.5*nu) * (F(t + mu*dt, x1) 
			  										+ F(t + (mu - nu)*dt, x2));
			  return ret.eval();
	  		};
	  
 	  newtonSolveY1(y1, un, dt, t, tol, maxIter);
	  newtonSolveY2(y2, un, y1, dt, t, tol, maxIter);
	  
	  return G(y1, y2);
	}

	///
	/// Compute N timesteps of DIRK(2,3)
	/// @param[in/out] u should be a vector of size 5, where each
	///                component is a vector of size N+1. u[i][0]
	///                should have the initial value stored before
	///                calling the funtion
	///
	/// @param[out] time should be of length N+1
	///
	/// @param[in] T the final time
	/// @param[in] N the number of timesteps
	///
	//----------------DirkStart----------------
	void solve(std::vector<std::vector<double> >& u, std::vector<double>& time,
		double T, int N) {

		const double dt = T / N;
		double tn;

		VXd un(dimens);

		un << u[iS][0], u[iE][0], u[iI][0], u[iR][0];

		// Your main loop goes here. At iteration n,
		// 1) Find Y_1 with newtonSolveY1 (resp. Y2)
		// 2) Compute U^{n+1} with F(Y1), F(Y2)
		// 3) Write the values at u[...][n] 
		// 4) Compute D and write time[n]
		
		for (int i = 1; i <= N; i++) {
		  tn = (i-1)*dt;
		  un = dirk23_step(un, tn, dt);
		  time[i] = tn;
		  for (int j = 0; j < dimens; j++)
		    u[j][i] = un(j);
		  coronaOutbreak.computeD(u, dt, i);
		}


	}
	//----------------DirkEnd----------------

private:

	const double mu = 0.5 + 0.5 / sqrt(3);
	const double nu = 1. / sqrt(3);
	
	static constexpr int maxIter = 100;
	static constexpr double tol = 1e-10;
	static constexpr int iS = 0, iE = 1, iI = 2, iR =3, iD = 4;

	CoronaOutbreak coronaOutbreak;

}; // end class DIRKSolver
