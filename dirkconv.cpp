#include "writer.hpp"
#include <Eigen/Core>
#include <Eigen/LU>
#include <vector>
#include <iostream>
#include <chrono>
#include "coronaoutbreak.hpp"
#include "dirksolver.hpp"

template<typename T>
using vec = std::vector<T>;

using clk = std::chrono::high_resolution_clock;
using timep = std::chrono::time_point<clk>;
using dur = std::chrono::duration<double>;

static inline int
N(int i) {
  return 200*(1<<i);
}

static constexpr int iD = 4;

//----------------mainBegin----------------
int main(int argc, char** argv) {

    double T = 450;
    CoronaOutbreak outbreak(0,0,0,0,0,0.03,0.02,0,0);
    std::vector<double> u0(5);
    u0[0] = 500;
    u0[1] = 0;
    u0[2] = 0;
    u0[3] = 0;
    u0[4] = 0;
    
    // Compute the exact solution for the parameters above
    std::vector<double> exact = outbreak.computeExactNoCorona(T, u0[0]);

    // Initialize solver object for the parameters above
    DIRKSolver dirkSolver(outbreak);

	timep t;

    int minExp = 0;
    int maxExp = 8;
    int countExponents = maxExp - minExp +1;
    std::vector<double> numbers(countExponents);
    std::vector<double> walltimes(countExponents);
    std::vector<double> errors(countExponents);
	vec<vec<double>> u(5);
	vec<double> time(N(maxExp));

	for (int i = 0; i < 5; i++) {
	  auto& e = u[i];
	  e = vec<double>(N(maxExp));
	  e[0] = u0[i];
	}

	for (int i = minExp; i <= maxExp; i++) {
	  t = clk::now();
	  dirkSolver.solve(u, time, T, N(i));
	  dur d = clk::now() - t;
	  walltimes[i - minExp] = d.count();
	  numbers[i - minExp] = N(i);
	  errors[i - minExp] = std::abs(u[iD][N(i)] - exact[iD]);
	}

    writeToFile("numbers.txt", numbers);
    writeToFile("errors.txt", errors);
    writeToFile("walltimes.txt", walltimes);

}
//----------------mainEnd----------------
