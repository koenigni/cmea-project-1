#pragma once
#include <functional>
#include <Eigen/Core>
#include <Eigen/LU>
#include <vector>
#include <iostream>

class CoronaOutbreak {
  private:
	static constexpr int iS = 0, iE = 1, iI = 2, iR =3, iD = 4;

	inline double H_f(double S, double E, double I, double R) {
	  return this->delta*(S + E + I + R) + this->sigma*I;
	}
	
  public:

    ///
    /// Evaluates right hand side of the system of ODEs U' = F(t,U)
    ///    S' = Pi*S - alpha*S*I - beta*S*E - delta*S + d*R
    ///    E' = alpha*S*I + beta*S*E - e*E - gamma *E - delta*E
    ///    I' = e*E -f*I - (sigma +delta) *I
    ///    R' = f*I + gamma *E - d*R - delta *R
    /// @param[in/out] F evaluation of RHS
    /// @param[in] U VectorXd with [S,E,I,R]
    /// @param[in] t current time
    ///
    //----------------FStart----------------
    void computeF(Eigen::VectorXd& F, double t, Eigen::VectorXd U) {
        double S = U[0];
        double E = U[1];
        double I = U[2];
        double R = U[3];

        F[0] = Pi * S - alpha(t) * S * I - beta(t) * S * E - delta * S + d * R;
        F[1] = alpha(t) * S * I + beta(t) * S * E  - e * E - gamma * E - delta * E;
        F[2] = e * E - f * I - sigma * I - delta * I;
        F[3] = f * I + gamma * E - d * R - delta * R; 
    }
    //----------------FEnd---------------

    ///
    /// Computes Jacobian matrix of F
    /// @param[in/out] J Jacobian matrix
    /// @param[in] t time
    /// @param[in] U VectorXd with [S,E,I,R]
    ///
    //----------------JFStart----------------
    void computeJF(Eigen::MatrixXd& J, double t, Eigen::VectorXd U) {
      double S = U[iS];
      double E = U[iE];
      double I = U[iI];
      double R = U[iR];

	  J(iS,iS) = Pi - alpha(t) * I - beta(t) * E - delta;
	  J(iS,iE) = -beta(t) * S;
	  J(iS,iI) = -alpha(t) * S;
	  J(iS,iR) = d;

	  J(iE,iS) = alpha(t) * I + beta(t) * E;
	  J(iE,iE) = beta(t) * S - e - gamma - delta;
	  J(iE,iI) = alpha(t) * S;
	  J(iE,iR) = 0;

	  J(iI,iS) = 0;
	  J(iI,iE) = e;
	  J(iI,iI) = -f - sigma - delta;
	  J(iI,iR) = 0;

	  J(iR,iS) = 0;
	  J(iR,iE) = gamma;
	  J(iR,iI) = f;
	  J(iR,iR) = -d - delta;
    }
    //----------------JFEnd----------------

    ///
    /// Computes Dead class D
    /// @param[in/out] u vector of vectors of variables. Dimensions: 5 x # time steps
    /// @param[in] dt time step, n integer corresponding to t_n+1.
    ///
    //----------------DStart----------------
    void computeD(std::vector< std::vector<double> >& u, const double dt, int n) {
	  int m = n-1;

	  if (m >= 0) 
		u[iD][n] = u[iD][m] + (dt/2.0) * (H_f(u[0][n], u[1][n], u[2][n], u[3][n]) + H_f(u[0][m], u[1][m], u[2][m], u[3][m]));
    }
    //----------------DEnd----------------

    double alpha(double t) {
        return a * (1. - r * t / (t + 1.));
    }

    double beta(double t) {
        return b * (1. - r * t / (t + 1.));
    }

    CoronaOutbreak() = default;

    CoronaOutbreak(double a_, double b_, double d_, double e_, double f_, 
        double Pi_, double delta_, double gamma_, double sigma_, double r_ = 0) :
        a(a_), b(b_), d(d_), e(e_), f(f_), Pi(Pi_), delta(delta_), gamma(gamma_), sigma(sigma_), r(r_) {
        // Empty
    }


    std::vector<double> computeExactNoCorona(double t, double S0) {
        std::vector<double> result(4);
        result[0] = S0 * std::exp( (Pi - delta) * t);
        result[1] = 0.;
        result[2] = 0.;
        result[3] = 0.;
        result[4] = (delta * S0 / (Pi - delta)) * ( std::exp( (Pi - delta) * t) - 1. );
        std::cout.precision(17);
        std::cout << "Exact solution: " << result[0] << " " << result[1]
            << " " << result[2] << " " << result[3] << " " << result[4] << std::endl;
        return result;
    }


private:

    const double r = 0.01;

    //! Birth rate
    const double Pi = 3e-5;

    //! Death rate
    const double delta = 2e-5;
    const double gamma = 1.5e-5;
    const double sigma = 2.5e-3;

    // The magic Garland constants:
    const double a = 1.5e-3;
    const double b = 0.3e-3;
    const double d = 2e-3;
    const double e = 0.5;
    const double f = 0.5;
};
